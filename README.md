## Overview

Small project that aims to solve the following tasks:

- Given a dataset a model can be trained using a event based architecure to facilitate long lasting operations.
- The training code produces an artifact that can be evaluated
- The dataset has a few images misslabed (wrong class assigned). They need to be identified.
- Relablle these images that were wrongly lablled.

The project is structured in the following way. 3 directories each of them contains a sub directory with the same name and `pyproject.toml` and `poetry.lock` and a `README.md`. The exception is the `services` directory which has 3 services that represent my choice of splitting the domain of the problem. The last one is training which works based on message queues.
This project uses poetry for depenecy management :heart: .

The stack:

- Pytorch - training, evaluating the model
- Weights and Biases - experiement tracking
- Fast API - endpoints
- RabbitMQ(Pika) - event driven architecture
- Weaviate - vector databases to hold the embeddings for image comparisson and retrival
- Docker - containerisation

## Notebooks

Client notebook does most of the client calls to the endpoints and displays some of the results from weights and biases. \
These calls map to the following: \

- Put a message to a queue to where a consumer which listens to events kicks off the training. A notebook cell listens for updates from trainer on a different queue.

- Evaluation of the model is done via a REST API call that evaluates the model and sends back a rudimentary performance report. The service evaluates and creates a table in weights and biases which can be used as additional content for evaluation. The call is made to a docker conatainer that's waiting to receive requests.

- The request for similar images is done via GET REST API call which selects the most similar instances and retreives the metadata necessary to identify them. This is a call to a vector databse and under the hood the similarity is computed via cosine similarity.

- Update a label of an image which is PUT REST API call. Update here means changing some fields values in the db and on disk.

## Pipelines

Contains most of the bits requiered to train and evaluate a model and training. Performance monitoring and experiemnt management is done via Weights and Biases.
Ideally this could be a library that can be shared with other services.

## Services

### Data Similarity

Emedding creation happens unfortunately by running a couple manual scripts. One defines the schema, another converters using VGG 19 (see docker compose file) all the training images to embeddings storing them to the db. They all live in `/scripts`

The other two endpoints are computing similarity and for relabelling.

### Evaluation

Endpoint performs evaluation and pushes to weights and biases a table with images, predictions, groundthruths. Responses with metrics like accuracy and the loss.

### Training Job

This listens for a message to be placed in the training queue, acknowledges it and then kicks off a training job throught it messages are published to another queue. A cell in Jupyter notebook listens for updates.
