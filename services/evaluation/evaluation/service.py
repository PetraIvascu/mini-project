"""
Module containing endpoints related to the training lifecycle of a model 
"""
import sys
import os
import torch

from typing import Any, Dict
from fastapi import FastAPI
from pydantic import BaseModel


sys.path.append(os.path.abspath("../../../pipelines/pipelines/"))

from constants import DATASET_LOCATION, MODEL_ARTIFACTS_LOCATION
from model_utils import evaluate, load_model, get_dataset_loader


class TrainRequest(BaseModel):
    config: Dict[str, Any]


class EvaluationRequest(BaseModel):
    model_name: str
    dataset_version: str


class EvaluationResponse(BaseModel):
    model_name: str
    dataset_version: str
    accuracy: float
    loss: float


app = FastAPI()


@app.get("/evaluate")
def evaluate_perfomance(evaluation_request: EvaluationRequest) -> EvaluationResponse:
    """
    Test the perfomance of a model and as a response returns details to associate the scores with some model details.
    """
    eval_inputs = evaluation_request.dict()
    model_name = eval_inputs["model_name"]
    dataset_version = eval_inputs["dataset_version"]

    model = load_model(location=os.path.join(MODEL_ARTIFACTS_LOCATION, model_name))
    test_dataset_laoder = get_dataset_loader(
        data_dir=os.path.join(DATASET_LOCATION, dataset_version, "test"),
        batch_size=128,
        shuffle=True,
    )
    accuracy, loss = evaluate(
        model=model,
        test_loader=test_dataset_laoder,
        device=torch.device("cpu"),
        log_performance=True,
        number_batches_log=32,
    )

    return EvaluationResponse(
        model_name=model_name,
        dataset_version=dataset_version,
        accuracy=accuracy,
        loss=loss,
    )
