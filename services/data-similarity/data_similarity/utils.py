"""
Module containing helper functions for the functionalities of the service
"""
import os
import json
import base64
from typing import Dict, List, Tuple

# TODO: move to dockerfile OR (preferably) external source that can mount the env var based on env (staging/prod)
os.environ["CLASS_NAME_ID_MAPPER"] = "class_name_to_id_mapping.txt"


def get_filenames(dataset_path: str) -> List[str]:
    """Create a collection of filepaths for the dataset path provided."""
    filepaths = []
    for dirpath, _, filenames in os.walk(dataset_path):
        for fn in filenames:
            if not fn.startswith("."):  # ignores files like .DS_Store
                filepaths.append(os.path.join(dirpath, fn))

    return filepaths


def get_class_name_id_mapper(dataset_location: str) -> Dict[str, str]:
    """Read the class name <-> id mapping file."""
    classname_id_mapper_filepath = os.path.join(
        dataset_location, os.environ["CLASS_NAME_ID_MAPPER"]
    )
    with open(classname_id_mapper_filepath) as f:
        mapper = json.loads(f.read())

    return mapper


def relabel_image(
    image_id: str, new_class_id: str, dataset_location: str
) -> Tuple[str, str]:
    """
    Changes location source image at the location composed from new class id and image id.
    This method assumes the dataset has no image duplications eg: image in train split might
    accidently appear in test too.
    """

    filepaths = get_filenames(dataset_location)

    for fpath in filepaths:
        fpath_image_name, _ = os.path.splitext(os.path.basename(fpath))
        if image_id == fpath_image_name:
            source_file = fpath
            break

    source_file_no_class = os.path.dirname(os.path.dirname(source_file))
    source_image_name = os.path.basename(source_file)
    destination_file = os.path.join(
        source_file_no_class, new_class_id, source_image_name
    )
    os.rename(source_file, destination_file)
    return source_image_name, destination_file


def encode_base64(img_path: str) -> str:
    """
    Reads image from path and converts to base64.
    """
    with open(img_path, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string.decode("utf-8")
