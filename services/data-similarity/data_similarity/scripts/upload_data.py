"""
Module that embedds a given set of images and uploads them to a vector database
"""
import sys
import os
import weaviate

from tqdm import tqdm

sys.path.append(os.path.abspath(".."))

from utils import encode_base64, get_class_name_id_mapper, get_filenames
from constants import CLASS_NAME, VECTOR_DB_URL

FILTER_SPLIT = "train"


def import_data(client: weaviate.client.Client):
    """
    Encode all images from train in base64 and add import them into vector db collection
    """
    dataset_location = os.environ.get("DATASET_LOCATION", None)

    if not dataset_location:
        raise EnvironmentError("Env variable DATASET_LOCATION not set!!!")

    dataset_location = os.path.join(
        dataset_location, "tenyks_ml_eng_task_907efe16-c40f-11ed-afa1-0242ac120002"
    )
    filepaths = get_filenames(dataset_location)
    filtered_paths = [fp for fp in filepaths if FILTER_SPLIT in fp]

    client.batch.configure(
        batch_size=100,
        dynamic=True,
        timeout_retries=3,
        callback=None,
    )
    with client.batch as batch:
        for path in tqdm(filtered_paths):
            id_to_class_name = get_class_name_id_mapper(dataset_location)
            class_number = os.path.basename(os.path.dirname(path))
            class_name = id_to_class_name[class_number]
            encoded_img = encode_base64(path)
            data_properties = {
                "class_name": class_name,
                "class_number": int(class_number),
                "image": encoded_img,
                "filepath": path,
            }

            uuid = batch.add_data_object(data_properties, CLASS_NAME)

    print("Aggregate Collection ... \n")
    print(client.query.aggregate(CLASS_NAME).with_meta_count().do())


def main():
    client = weaviate.Client(VECTOR_DB_URL)

    # deletes the whole db !!!
    # client.schema.delete_class(CLASS_NAME)

    import_data(client)
    print("The objects have been uploaded to Weaviate.")


if __name__ == "__main__":
    main()
