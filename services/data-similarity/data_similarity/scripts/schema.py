"""
Script that defines and creates the schema for the vector database
"""
import sys
import os
from typing import Any, Dict
import weaviate


sys.path.append(os.path.abspath(".."))

from constants import CLASS_NAME, VECTOR_DB_URL


SCHEMA = {
    "classes": [
        {
            "class": CLASS_NAME,
            "description": "Images of different entities: enemies and friends",
            "moduleConfig": {"img2vec-neural": {"imageFields": ["image"]}},
            "vectorIndexType": "hnsw",
            "vectorizer": "img2vec-neural",  # the img2vec-neural Weaviate module
            "properties": [
                {
                    "name": "class_name",
                    "dataType": ["string"],
                    "description": "name of class [enemy/friend]",
                },
                {
                    "name": "class_number",
                    "dataType": ["number"],
                    "description": "Label number for the given image.",
                },
                {
                    "name": "image",
                    "dataType": ["blob"],
                    "description": "image",
                },
                {
                    "name": "filepath",
                    "dataType": ["string"],
                    "description": "filepath of the images",
                },
            ],
            "vectorIndexType": "hnsw",
            "vectorizer": "img2vec-neural",
        }
    ]
}


def main(schema: Dict[str, Any]):
    """
    Adding the schema declared to global scope to vector database.
    Schema has the following properties:
        * class_name
        * class_number
        * image (base64)
        * filepath
    """
    client = weaviate.Client(VECTOR_DB_URL)
    client.schema.create(schema)
    print("The schema has been defined.")
    schema = client.schema.get()
    print(schema)


if __name__ == "__main__":
    main(SCHEMA)
