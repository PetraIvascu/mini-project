"""
Module containing endpoints related to dataset correctness 
"""
import os
import logging
import weaviate
from typing import List
from uuid import UUID
from fastapi import FastAPI
from pydantic import BaseModel

from constants import CLASS_NAME, VECTOR_DB_URL
from utils import encode_base64, get_class_name_id_mapper, relabel_image

LOGGER = logging.getLogger(name="similarity")
LOGGER.setLevel(logging.INFO)
logging.basicConfig()


class SimilarityRequest(BaseModel):
    image_id: UUID
    dataset_version: str


class SimilarityResponse(BaseModel):
    image_id: UUID
    entry_db_id: UUID
    class_number: int
    class_name: str


class UpdateLabelRequest(BaseModel):
    image_id: UUID  # makes sure that only the id of the image is passed
    entry_db_id: UUID
    new_label: str
    dataset_version: str


app = FastAPI()


@app.get("/datasets/similarity")
def similarity(similarity_request: SimilarityRequest) -> List[SimilarityResponse]:
    """
    Makes a query to the vector database to find all the similar embeddings with the images provided
    """

    dataset_location = os.environ.get("DATASET_LOCATION", None)
    if not dataset_location:
        raise EnvironmentError("Env variable DATASET_LOCATION not set!!!")

    similarity_request = similarity_request.dict()

    image_id = str(similarity_request["image_id"].hex)
    image_id = f"{image_id}.jpg"

    dataset_version = similarity_request["dataset_version"]
    image_location = os.path.join(
        dataset_location, dataset_version, "train", "0", image_id
    )

    client = weaviate.Client(VECTOR_DB_URL)
    img = encode_base64(image_location)
    source_image = {"image": img}

    weaviate_results = (
        client.query.get(CLASS_NAME, ["filepath", "class_name", "class_number"])
        .with_near_image(source_image, encode=False)
        .with_additional(["id"])
        .do()
    )

    query_response = weaviate_results["data"]["Get"][CLASS_NAME]
    LOGGER.info(f"Retrieved {len(query_response)} objects")

    similarity_response = []

    for response in query_response:
        response_image_id, _ = os.path.splitext(os.path.basename(response["filepath"]))
        entry_db_id = response["_additional"]["id"]
        similarity_response.append(
            SimilarityResponse(
                image_id=UUID(response_image_id).hex,
                entry_db_id=UUID(entry_db_id),
                class_number=response["class_number"],
                class_name=response["class_name"],
            )
        )
    return similarity_response


@app.put("/datasets/relabel")
def update_label(update_label_request: UpdateLabelRequest):
    """
    Given an image id, new label an update on the dataset for that image will be performed.
    Assumes there might be more dataset, therefore it has to be specified.

    TODO: "soft" relabeling in case of mistakes to have a history of every action so tracing
    back can be possible.
    First version solution: a file that logs some strings.
    """
    dataset_location = os.environ.get("DATASET_LOCATION", None)

    if not dataset_location:
        raise EnvironmentError("Env variable DATASET_LOCATION not set!!!")

    label = update_label_request.dict()
    image_id = str(label["image_id"].hex)
    entry_db_id = str(label["entry_db_id"])
    new_label = label["new_label"]
    dataset_version = label["dataset_version"]
    dataset_location = os.path.join(dataset_location, dataset_version)

    classname_id_mapper = get_class_name_id_mapper(dataset_location)
    class_ids = classname_id_mapper.keys()
    class_names = classname_id_mapper.values()

    if not new_label in class_ids and not new_label in class_names:
        raise ValueError(
            f"Label Provided: {new_label} not allowed: {classname_id_mapper}"
        )

    if new_label in class_names:
        idx = list(class_names).index(new_label)
        new_class_id = list(class_ids)[idx]
    else:
        new_class_id = new_label

    src_path, dst_path = relabel_image(
        image_id=image_id,
        new_class_id=new_class_id,
        dataset_location=dataset_location,
    )
    LOGGER.info(f"Source path {src_path} -> Destination path {dst_path}")

    client = weaviate.Client(VECTOR_DB_URL)
    client.data_object.update(
        data_object={
            "filepath": dst_path,
            "class_name": classname_id_mapper[new_class_id],
            "class_number": int(new_class_id),
        },
        class_name=CLASS_NAME,
        uuid=entry_db_id,
    )
    LOGGER.info(f"Replaced object {entry_db_id}")
