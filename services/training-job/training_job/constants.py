"""
Module that stores variopus constants used within service
"""
import pika

from dataclasses import dataclass
from dataclasses_json import dataclass_json

# message queue
HOST_NAME = "localhost"
PORT = 5672
CREDENTIALS = pika.PlainCredentials("user", "password")
TRAIN_QUEUE_NAME = "train"

TRAIN_FINISH_EXCHANGE = "train_finish_exchange"
TRAIN_FINISH_ROUTING_KEY = "train_finish"

# training
WANB_PROJECT = "save-world"


@dataclass_json
@dataclass
class Configuration(object):
    seed: int = 1
    log_interval: int = 10
    batch_size: int = 128
    epochs: int = 10
    learning_rate: float = 0.001
    dataset_version: str = "tenyks_ml_eng_task_907efe16-c40f-11ed-afa1-0242ac120002"
    model_artifact_name: str = "HeroNet"
    num_classes: int = 2
