"""
Contains additional logic to support publishing a message to a queue
"""
import json
import pika

from constants import (
    CREDENTIALS,
    HOST_NAME,
    PORT,
    TRAIN_FINISH_ROUTING_KEY,
    TRAIN_FINISH_EXCHANGE,
)


def push_message(message: str):
    """
    Push a meesage to another queue where client is listening for training finish
    """
    conn_partameters = pika.ConnectionParameters(
        host=HOST_NAME, port=PORT, virtual_host="/", credentials=CREDENTIALS
    )
    connection = pika.BlockingConnection(parameters=conn_partameters)
    channel = connection.channel()

    channel.basic_publish(
        exchange=TRAIN_FINISH_EXCHANGE,
        routing_key=TRAIN_FINISH_ROUTING_KEY,
        body=json.dumps(message),
    )

    connection.close()
