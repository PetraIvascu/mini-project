"""
Module contains listen to queue functionality
"""
import logging
import pika
import json


from constants import (
    CREDENTIALS,
    HOST_NAME,
    PORT,
    TRAIN_QUEUE_NAME,
    Configuration,
)
from train import training_task


LOGGER = logging.getLogger(name="consumer")
LOGGER.setLevel(logging.INFO)
logging.basicConfig()


def callback(ch, method, properties, body):
    config = json.loads(body)
    training_task(cfg=Configuration(**config))


def listen_for_message():
    """
    Connects to the queue where it listens for a message to pe pushed, the does dequeue
    """
    LOGGER.info(f"Listening for message")
    conn_params = pika.ConnectionParameters(
        host=HOST_NAME, port=PORT, virtual_host="/", credentials=CREDENTIALS
    )
    connection = pika.BlockingConnection(parameters=conn_params)
    channel = connection.channel()

    channel.basic_consume(
        queue=TRAIN_QUEUE_NAME, on_message_callback=callback, auto_ack=True
    )
    channel.start_consuming()


if __name__ == "__main__":
    listen_for_message()
