"""
Script used to perfomr training of model
"""
import sys
import os
import json
import logging
import math
import torch
import torch.nn.functional as F
import wandb
import torchvision

from torch import nn
from torch import optim
from torchvision import models

from pipelines.model_utils import compute_accuracy, evaluate, get_dataset_loader
from publisher import push_message
from constants import (
    WANB_PROJECT,
    Configuration,
)


sys.path.append(os.path.abspath("../../../pipelines/pipelines/"))


LOGGER = logging.getLogger(name="model_training")
LOGGER.setLevel(logging.INFO)
logging.basicConfig()


def train(
    model: torchvision.models.resnet.ResNet,
    device: torch.device,
    train_loader: torch.utils.data.dataloader.DataLoader,
    optimizer: torch.optim.Adam,
    epoch: int,
    log_interval: int,
) -> float:
    """
    Performs the training of model. Records performance to weights and biases.
    """
    model.train()
    accuracy = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()  # clear gradient
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0:
            LOGGER.info(
                "Train Epoch: {} [{}/{} ({:.0f}%)]\tloss={:.4f}".format(
                    epoch,
                    batch_idx * len(data),
                    len(train_loader.dataset),
                    100.0 * batch_idx / len(train_loader),
                    loss.item(),
                )
            )

        accuracy += compute_accuracy(
            logits=output.data,
            targets=target,
        )

        wandb.log({"train_loss": loss})

    return accuracy


def get_model(num_classes: int) -> torchvision.models.resnet.ResNet:
    """
    Model for which we will perform finetuning.
    """
    model = models.resnet18(weights="DEFAULT")
    num_features = model.fc.in_features
    model.fc = nn.Linear(num_features, num_classes)
    return model


def training_task(cfg: Configuration):
    """
    Given a configuration we define the main flow of the training process.
    """
    LOGGER.info(f"Configureation {cfg}")

    dataset_location = os.environ.get("DATASET_LOCATION", None)
    model_location = os.environ.get("MODEL_LOCATION", None)

    if not dataset_location:
        raise EnvironmentError("Env variable DATASET_LOCATION not set!!!")

    if not model_location:
        raise EnvironmentError("Env variable MODEL_LOCATION not set!!!")

    wandb.init(
        project=WANB_PROJECT, mode="offline"
    )  # having some errors with loggin to the web console
    wandb.config.update(json.loads(cfg.to_json()))

    torch.manual_seed(cfg.seed)

    use_cuda = torch.cuda.is_available()
    LOGGER.info(f"Use cuda {use_cuda}")
    device = torch.device("cuda" if use_cuda else "cpu")

    dataset_location = os.path.join(dataset_location, cfg.dataset_version)
    training_data_loader = get_dataset_loader(
        data_dir=os.path.join(dataset_location, "train"),
        batch_size=cfg.batch_size,
        shuffle=True,
    )
    LOGGER.info(
        f"Loaded train data - batch size: {training_data_loader.batch_size} number of data points: {len(training_data_loader.dataset)}"
    )

    test_data_loader = get_dataset_loader(
        data_dir=os.path.join(dataset_location, "test"),
        batch_size=cfg.batch_size,
        shuffle=True,
    )
    LOGGER.info(
        f"Loaded validation data - batch size: {test_data_loader.batch_size}  number of data points: {len(test_data_loader.dataset)}"
    )

    model = get_model(num_classes=cfg.num_classes)
    LOGGER.info("Loaded model ...")

    optimizer = optim.Adam(model.parameters(), lr=cfg.learning_rate)

    accuracies = []
    number_batches = math.ceil(
        len(training_data_loader.dataset) / training_data_loader.batch_size
    )

    for epoch in range(0, cfg.epochs):
        LOGGER.info(f"Epoch: {epoch}")
        train_accuracy = train(
            model, device, training_data_loader, optimizer, epoch, cfg.log_interval
        )
        wandb.log({"train_accuracy": train_accuracy})
        LOGGER.info(
            "Train Epoch: {} \t train set accurcay={:.4}".format(
                epoch,
                train_accuracy / number_batches,
            )
        )

        val_accuracy, validation_loss = evaluate(
            model=model, test_loader=test_data_loader, device=device
        )
        LOGGER.info("\n validation set accuracy={:.4f}\n".format(val_accuracy))
        LOGGER.info("\n validation set loss={:.4f}\n".format(validation_loss))
        wandb.log({"validation_accuracy": val_accuracy})
        wandb.log({"validation_loss": validation_loss})

        accuracies.append(val_accuracy)
        push_message(
            message=f"Epoch {epoch + 1} out of {cfg.epochs} has finished",
        )

    model_location_pt = os.path.join(
        model_location,
        f"{cfg.model_artifact_name}.pt",
    )

    LOGGER.info(f"Saving model to {model_location_pt}")
    torch.save(model.state_dict(), model_location_pt)

    model_location_jit = os.path.join(
        model_location,
        f"{cfg.model_artifact_name}.jit",
    )

    LOGGER.info(f"Convert model to torch script")
    model_scripted = torch.jit.script(model)

    LOGGER.info(f"Saving model to {model_location_jit}")
    model_scripted.save(model_location_jit)

    LOGGER.info(f"End of training ...")
    push_message(message="End of training")

    LOGGER.info(f"Pushed finished message")
