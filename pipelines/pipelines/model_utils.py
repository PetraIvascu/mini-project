"""
Module contianing common helper functionalities for model training and evlauation
"""
from typing import Tuple
import math
import wandb
import torch
import torch.nn.functional as F

from torchvision import datasets, transforms


def create_performance_table(images, predictions, targets):
    """
    Performance for eval will be logged into a wandb table
    """
    columns = ["id", "image", "predicted_class", "ground_truth"]

    table_content = [
        [idx, wandb.Image(obj[0], mode="RGB"), obj[1], obj[2]]
        for idx, obj in enumerate(zip(images, predictions, targets))
    ]

    table = wandb.Table(data=table_content, columns=columns)
    wandb.log({"table_key": table})


def compute_accuracy(logits: torch.Tensor, targets: torch.Tensor) -> float:
    """
    Calculate the accuracy over a set of entries.
    """
    _, predicted = torch.max(logits, 1)
    correct = (predicted == targets).sum().item()
    accuracy = float(correct) / len(targets)
    return accuracy


def evaluate(
    model: torch.jit._script.RecursiveScriptModule,
    test_loader: torch.utils.data.dataloader.DataLoader,
    device: torch.device,
    log_performance: bool = False,
    number_batches_log: int = 0,
) -> Tuple[float, float]:
    """
    Run evaluation on a portion of data.
    """
    test_loss = 0
    accuracy = 0
    log_counter = 0

    number_batches = math.ceil(len(test_loader.dataset) / test_loader.batch_size)
    model.eval()
    if log_performance:
        wandb.init(project="save-world")
    with torch.no_grad():
        for images, targets in test_loader:
            images, targets = images.to(device), targets.to(device)
            outputs = model(images)
            test_loss += F.nll_loss(outputs, targets, reduction="sum").item()
            accuracy += compute_accuracy(
                logits=outputs.data,
                targets=targets,
            )
            if log_performance and log_counter < number_batches_log:
                _, predictions = torch.max(outputs.data, 1)
                create_performance_table(
                    images=images, predictions=predictions, targets=targets
                )

                log_counter += 1

    test_loss /= len(test_loader.dataset)
    total_accuracy = accuracy / number_batches

    return total_accuracy, test_loss


def get_dataset_loader(
    data_dir: str, batch_size: int = 32, shuffle: bool = True
) -> torch.utils.data.dataloader.DataLoader:
    """
    Create a loader for dataset found in dataset directory
    """
    transform = transforms.Compose([transforms.ToTensor()])
    dataset = datasets.ImageFolder(data_dir, transform=transform)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=shuffle
    )
    return dataloader


def load_model(location: str) -> torch.jit._script.RecursiveScriptModule:
    """
    Load the converted model found at a given location.
    """
    model = torch.jit.load(location)
    return model
